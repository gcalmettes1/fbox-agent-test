# type: ignore

import json
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from data import DataModel
from matplotlib.colors import to_rgba_array
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import accuracy_score

from fbox.agent import Agent, Render

celery_app = Agent._app_task_queue


##################
# Agent template
##################

# Please rename your agent as you like
demo = Agent("demo", static_folder="static")

# SDG classifier as model.
# Warm start so we can do online learning.
MyClassifier = SGDClassifier(warm_start=True)

# Those are just mockup of persistant data
dataModel = None
dataBatch = None
allData = pd.DataFrame(columns=["x1", "x2", "y"])
allAccuracies = pd.DataFrame(columns=["time", "accuracy"])
count = 0
colors = ["#EDC951", "#CC333F", "#00A0B0"]
selected_color = 0

# for feature space visualization
xx, yy = np.meshgrid(np.linspace(-8, 8, 100), np.linspace(-8, 8, 100))


def reset():
    global dataBatch, dataModel, allData, allAccuracies, count, xx, yy
    dataModel = None
    dataBatch = None
    allData = pd.DataFrame(columns=["x1", "x2", "y"])
    count = 0
    allAccuracies = pd.DataFrame(columns=["time", "accuracy"])
    xx, yy = np.meshgrid(np.linspace(-8, 8, 100), np.linspace(-8, 8, 100))


@demo.register(use_async=False)
def get_data():
    global dataBatch, dataModel, allData, count, xx, yy

    if dataModel is None:
        dataModel = DataModel(initial_sample_size=20)
    dataBatch = dataModel.get_batch()

    while True:
        try:
            X, y = next(dataBatch)
            if count < 1:
                # first fit
                MyClassifier.fit(X, y)
            else:
                MyClassifier.partial_fit(X, y)
            count += 1

            mesh = MyClassifier.predict(np.c_[xx.ravel(), yy.ravel()])

            data = pd.DataFrame(np.c_[X, y], columns=["x1", "x2", "y"])
            allData = allData.append(data)
            now = datetime.now()  # current date and time
            data_time = now.strftime("%m/%d/%Y, %H:%M:%S")
            response = {
                "data": json.loads(allData.to_json(orient="records")),
                "mesh": mesh.tolist(),
                "time": data_time,
            }
            yield response
        except StopIteration:
            break


@demo.register_html(methods=["GET"])
def get_coefs_table():
    coefs = pd.DataFrame(MyClassifier.coef_, columns=["alpha", "beta"])

    html = """
    <table>
        <thead>
            <th>Align</th>
            <th>Alpha</th>
            <th>Beta</th>
        </thead>
        </tbody>

    """
    aligns = ["left", "zero", "mid"]
    for align in aligns:
        row = "<tr><th>{}</th>".format(align)
        for col in coefs:
            s = coefs.loc[:, col].copy()
            s.name = ""
            row += "<td>{}</td>".format(
                s.to_frame()
                .style.bar(align=align, color=["#d65f5f", "#5fba7d"], width=100)
                .render()
            )  # testn['width']
        row += "</tr>"
        html += row

    html += """
    </tbody>
    </table>"""

    return html


@demo.register_html(methods=["GET"])
def get_accuracy_table():
    return allAccuracies.tail().to_html()


@demo.register_html
def interface():
    return Render.html("static/index.html")


@demo.register(use_async=False)
def reset_data():
    reset()
    return "Data reset !"


@demo.register(use_async=False)
def select_color(c: int):
    global selected_color
    selected_color = c
    return "Color changed !"


@demo.register_stream(media_type="image/png")
def get_accuracy_png():
    global allAccuracies

    fig, ax = plt.subplots(figsize=(6, 4))

    try:
        predictions = MyClassifier.predict(allData.loc[:, ["x1", "x2"]])
        acc = accuracy_score(allData.loc[:, ["y"]], predictions)
        accuracy = pd.DataFrame(
            np.c_[pd.to_datetime(datetime.now()), acc], columns=["time", "accuracy"]
        )
        allAccuracies = allAccuracies.append(accuracy)

        ax = allAccuracies.plot(x="time", y="accuracy", ax=ax, style="o-")
    except Exception:
        pass
    ax.set_ylim([0.5, 1])
    return Render.mpl(fig, format="png")


@demo.register_html
def get_accuracy_svg():
    global allAccuracies

    fig, ax = plt.subplots(figsize=(6, 4.5))
    print(to_rgba_array(colors[1]))
    try:
        predictions = MyClassifier.predict(allData.loc[:, ["x1", "x2"]])
        acc = accuracy_score(allData.loc[:, ["y"]], predictions)
        accuracy = pd.DataFrame(
            np.c_[pd.to_datetime(datetime.now()), acc], columns=["time", "accuracy"]
        )
        allAccuracies = allAccuracies.append(accuracy)

        ax = allAccuracies.plot(
            x="time",
            y="accuracy",
            ax=ax,
            style="o-",
            color=to_rgba_array(colors[selected_color])[0][:3],
        )
    except Exception:
        pass
    ax.set_ylim([0.5, 1.05])
    ax.set_ylabel("Accuracy")
    fig.tight_layout()
    return Render.mpl(fig, format="svg")
