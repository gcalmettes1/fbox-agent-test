FROM ubuntu:18.04

RUN apt-get update && apt-get install \
  -y software-properties-common git-core
RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt-get install -y --no-install-recommends \
    python3.7 python3.7-venv
RUN rm -rf /var/lib/apt/lists/*


ENV VIRTUAL_ENV=/opt/venv
RUN python3.7 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN python3.7 -m pip install --upgrade pip

# for click
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

ARG GITLAB_NPA
ARG GITLAB_NPA_PW

# embed current agent
ADD . /  /app/
RUN rm -rf /app/Dockerfile

RUN pip3 install setuptools wheel
# installing latest develop for the moment. Will have to be updated.
# add #egg=agent[cv] at the end if you want the extra computer-vision dependencies
RUN pip3 install /app/agent-0.0.1-py3-none-any.whl
RUN pip3 install -r /app/requirements.txt
RUN pip3 install uvicorn

WORKDIR /app

EXPOSE 8000

# somehow need to pass via bash for being able to ctrl-c
CMD ["bash", "-c", "uvicorn --use-colors --port 8000 --host 0.0.0.0 agent:Agent._app_api"]
