# type: ignore

import numpy as np


class BlobGenerator:
    def __init__(
        self,
        n_features=2,
        initial_sample_size=30,
        batch_sample_size=1,
        centers=None,
        cluster_std=1.0,
        center_box=(-10.0, 10.0),
        shuffle=True,
    ):
        self.center_box = center_box
        (self.centers, self.n_centers, self.n_features) = self.get_centers(
            centers, center_box, n_features
        )
        self.initial_sample_size = initial_sample_size
        self.batch_sample_size = batch_sample_size
        self.cluster_std = self.get_centers_std(
            cluster_std, self.centers, self.n_centers,
        )
        self.shuffle = shuffle

    @staticmethod
    def get_centers(centers, center_box, n_features):
        if centers is None:
            centers = 3
        if isinstance(centers, int):
            n_centers = centers
            centers = np.random.uniform(
                center_box[0], center_box[1], size=(n_centers, n_features)
            )
        else:
            centers = np.array(centers)
            n_features = centers.shape[1]
            n_centers = centers.shape[0]

        return centers, n_centers, n_features

    @staticmethod
    def generate_data(centers, n_features, n_samples, n_centers, cluster_std, shuffle):
        X = []
        y = []

        if isinstance(n_samples, list):
            n_samples_per_center = n_samples
        else:
            groups = [np.random.choice(range(n_centers)) for _ in range(n_samples)]
            n_samples_per_center = [groups.count(i) for i in range(n_centers)]

        for i, (n, std) in enumerate(zip(n_samples_per_center, cluster_std)):
            X.append(np.random.normal(loc=centers[i], scale=std, size=(n, n_features)))
            y += [i] * n

        X = np.concatenate(X)
        y = np.array(y)

        if shuffle:
            total_n_samples = np.sum(n_samples)
            indices = np.arange(total_n_samples)
            np.random.shuffle(indices)
            X = X[indices]
            y = y[indices]

        return X, y

    @staticmethod
    def get_centers_std(cluster_std, centers, n_centers):
        # stds: if cluster_std is given as list, it must be consistent
        # with the n_centers
        if hasattr(cluster_std, "__len__") and len(cluster_std) != n_centers:
            raise ValueError(
                "Length of `clusters_std` not consistent with "
                "number of centers. Got centers = {} "
                "and cluster_std = {}".format(centers, cluster_std)
            )

        if isinstance(cluster_std, float):
            cluster_std = np.full(len(centers), cluster_std)

        return cluster_std

    def generate(self):
        X, y = self.generate_data(
            self.centers,
            self.n_features,
            self.initial_sample_size,
            self.n_centers,
            self.cluster_std,
            self.shuffle,
        )
        yield X, y

        while True:
            X, y = self.generate_data(
                self.centers,
                self.n_features,
                self.batch_sample_size,
                self.n_centers,
                self.cluster_std,
                self.shuffle,
            )

            yield X, y


class DataModel:
    def __init__(self, initial_sample_size=40, batch_sample_size=3):
        self.initial_sample_size = initial_sample_size
        self.batch_sample_size = batch_sample_size
        self.data_generator = BlobGenerator(
            n_features=2,
            initial_sample_size=initial_sample_size,
            batch_sample_size=batch_sample_size,
            centers=None,
            cluster_std=1.0,
            center_box=(-8.0, 8.0),
            shuffle=True,
        ).generate()

    def reset(self, initial_sample_size=None, batch_sample_size=None):
        # reset data generator
        if initial_sample_size is not None:
            self.initial_sample_size = initial_sample_size
        if batch_sample_size is not None:
            self.batch_sample_size = batch_sample_size
        self.data_generator = BlobGenerator(
            n_features=2,
            initial_sample_size=self.initial_sample_size,
            batch_sample_size=self.batch_sample_size,
            centers=None,
            cluster_std=1.0,
            center_box=(-8.0, 8.0),
            shuffle=True,
        ).generate()
        # send new data
        return self.get_batch()

    def get_batch(self):
        X, y = next(self.data_generator)
        yield X, y
